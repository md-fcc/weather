My weather website to fulfill the [FreeCodeCamp challenge](https://www.freecodecamp.org/challenges/show-the-local-weather).

Live website: [Weather](https://md-fcc.gitlab.io/weather/)

I was reading about Model-View-Controller (MVC) frameworks, and decided to try implementing the idea for this website. I found that MVC can mean a lot of different things and comes in a lot of different flavors. However, the basic idea is generally to isolate responsibilities within the code. This website uses:
- WeatherModel - Stores weather data (temp, conditions)
- WeatherView - Displays the weather data in the HTML
- WeatherController - Instructs the WeatherModel to retrive data from the weather API.

I am using the Yahoo weather API for this website. FreeCodeCamp also provided their own [weather API](https://fcc-weather-api.glitch.me), but I was interested in learning about Yahoo.

My Javascript code uses a chain of promises to capture errors that may occur, if for example, the user denies getting lat/lon from the browser location, or data cannot be retrieved from the weather API.