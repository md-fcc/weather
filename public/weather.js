// -----------------------------------------------------------------------------
// --------------------------- Model -------------------------------------------
// -----------------------------------------------------------------------------

class WeatherModel {
    // WeatherModel is responsible for storing the weather data, and contains
    // methods for getting the data from the Yahoo weather API.
    
	constructor() {
		// initialize to fake/placeholder values
		this.temperature_degF = 99;
		this.currentDescription = "Mostly Beans and cheese";
		this.weatherImageUrl  = "img/4.gif";
		this.weatherTimestamp = "Sat, 12 Aug 2017 01:00 PM PDT";
		this.city = "";
		this.cityRegion = "";
	}
	

	getWeatherAtPosition(position) {
		// Get weather from Yahoo! weather API based on position lat,lon
		// returns a promise object of the weather data
		
		// store position info
		this.lat = position.coords.latitude;
		this.lon = position.coords.longitude;

		// Execute Weather Query
		let yahooWeatherURL = "https://query.yahooapis.com/v1/public/yql?",
		    weatherQuery = {
				q: "select * from weather.forecast where woeid in (SELECT woeid FROM geo.places WHERE text=\"(" + this.lat + "," + this.lon + ")\")",
				format: "json"
			};
		
		let weatherDataPromise = 
			$.getJSON(yahooWeatherURL, weatherQuery)
			 .done( (data) => {
				console.log( "done getting weather" );
				this.setWeatherData(data);  
			 })
			 .fail(function() {
				console.log( "error getting weather" );  // should be pushed up to the view as an event?
			 });
		return(weatherDataPromise);
	}


	setWeatherData(data) {
		// takes JSON results from Yahoo Weather Query and stores the info
		// into the WeatherModel
		this.weather = data.query.results; // all data
		
        this.currentCondition = this.weather.channel.item.condition;
        this.temperature_degF = Number(this.currentCondition.temp);
		this.currentDescription = this.currentCondition.text;
		this.weatherImageUrl = "http://l.yimg.com/a/i/us/we/52/" 
							 + this.weather.channel.item.condition.code 
							 + ".gif";

		this.weatherTimestamp = this.currentCondition.date;
		this.setCityRegion(this.weather.channel.location.city,
		                   this.weather.channel.location.region);
	}


	setCityRegion(city, region) {
		this.city = city;
		this.cityRegion = region;		
	}
}


// -----------------------------------------------------------------------------
// --------------------------- View -------------------------------------------
// -----------------------------------------------------------------------------

class WeatherView {
	// WeatherView is responsible for displaying the WeatherModel data 
    // in the HTML website.
    
	constructor(weatherModel) {
		this.model = weatherModel;
		this.temperature_view_unit = "F"; // default view of temperature in deg F

		// Bind Functions to Temperature Unit Toggler Buttons
		$("#f-units").click( () => {
			if (this.temperature_view_unit === "C") {
				this.temperature_view = this.model.temperature_degF;
				this.temperature_view_unit = "F";
				$("#f-units").toggleClass("selected-temp-unit unselected-temp-unit");
				$("#c-units").toggleClass("selected-temp-unit unselected-temp-unit");
				this.updateTempUnitText(this.temperature_view.toFixed(1));
			}
		});
	
		$("#c-units").click( () => {
			if (this.temperature_view_unit === "F") {
				this.temperature_view = 5 / 9 * (this.model.temperature_degF - 32);
				this.temperature_view_unit = "C";
				$("#f-units").toggleClass("selected-temp-unit unselected-temp-unit");
				$("#c-units").toggleClass("selected-temp-unit unselected-temp-unit");
				this.updateTempUnitText(this.temperature_view.toFixed(1));
			}
		});
	}
	

	updateTempUnitText(temp_deg) {
		$("#temperature").html(temp_deg + "&deg");
	}


	unhideHTML() {
		// Unhides all HTML elements with hidden class, hides spinner
		$("[class*='hidden']").removeClass("hidden");
		$("#waiting-spinner").addClass("hidden");
	}


	updateHTML() {
		this.updateTempUnitText(this.model.temperature_degF.toFixed(1));
		$("#weather-description").html(this.model.currentDescription);
		$("#weather-image").attr("src", this.model.weatherImageUrl);
		
		$("#timestamp").html("Temperature on:<br>" + this.model.weatherTimestamp);
		$("#weather-provider").html("Powered by Yahoo!");
		
		$("#city-name").html(this.model.city + ", " + this.model.cityRegion);
	}
	
}


// -----------------------------------------------------------------------------
// --------------------------- Controller --------------------------------------
// -----------------------------------------------------------------------------

class WeatherController {
	// WeatherController is responsible for calling the WeatherModel methods
    // for getting the weather data. WeatherController doesn't care about WeatherModel's implementation, 
    // the controller just instructs the Model to get the weather data for a particular lat/lon.
    //
    
	constructor(weatherModel, weatherView) {
		this.model = weatherModel;
		this.view = weatherView;
	}
	
	getLocation() {
		// Get lat,lon from browser
		return new Promise(function (resolve, reject) {
			navigator.geolocation.getCurrentPosition(resolve, reject);
		});
	}
	
	getWeather() {
		// Get weather based on position
		this.getLocation()
		.then((position) => {
			this.model.getWeatherAtPosition(position)
			.then( () => {
				this.view.unhideHTML();
				this.view.updateHTML();
			})
			.catch( (err) => {
				console.error(err.message);
				// Put fake data in model, indicating that can't get weather
				this.model.setCityRegion("Couldn't retrieve weather; here's a guess...", "");
				this.view.unhideHTML();
				this.view.updateHTML();
			})
		})
		.catch( (err) => {
			console.error(err.message);
			// Put fake data in model, indicating that can't get position
			this.model.setCityRegion("Couldn't find your location; here's a guess at the weather...", "");
			this.view.unhideHTML();
			this.view.updateHTML();
		})
	}
}


// -----------------------------------------------------------------------------
// --------------------------- Main --------------------------------------------
// -----------------------------------------------------------------------------

$(document).ready(function () {
	let weatherModel = new WeatherModel,
	    weatherView = new WeatherView(weatherModel),
	    weatherController = new WeatherController(weatherModel, weatherView);
	
	weatherController.getWeather();
});
